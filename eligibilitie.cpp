/*
 * eligibilitie.cpp
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#include<iostream>
#include"header.h"
#include<sstream>
#include<fstream>
#include<iomanip>
#include<vector>


eligibilitie::eligibilitie()
{
	eligib_course_name=" ";
	eligib_degree=" ";
	eligib_min_percentage=0;
}

eligibilitie::eligibilitie(string eligib_course_name,string degree,double min_percentage)
{
	this->eligib_course_name=eligib_course_name;
	this->eligib_degree=degree;
	this->eligib_min_percentage=min_percentage;
}

void eligibilitie::accepct_elig()
{
	cout<<"\nEnter course name :";
	cin>>this->eligib_course_name;
	cout<<"\nEnter course eligib. degrees :";
	cin>>this->eligib_degree;
	cout<<"\nEnter course name :";
	cin>>this->eligib_min_percentage;
}


void eligibilitie::display_elig()
{
	//cout<<setw(15)<<left<<this->eligib_course_name;
	cout<<setw(25)<<left<<this->eligib_degree;
	cout<<setw(13)<<left<<this->eligib_min_percentage;
}

string eligibilitie::get_eligib_course_name()
{
	return this->eligib_course_name;
}
string eligibilitie::get_eligib_degree_name()
{
	return this->eligib_degree;
}

int eligibilitie::get_eligib_persentage()
{
	return this->eligib_min_percentage;
}

//============ ** Global Functions ** ================


//loading eligibilities.csv in vector

void load_student_csv(vector<eligibilitie>& eligibilities) {
	// open file
	ifstream fin("eligibilities.csv");
	if(!fin)
	{
		cout<<"\nFile not opent "<<endl;
		return;
	}

	string line;
	// read line by line
	while(getline(fin,line))
	{
		// each line split tokens
		string tokens[3];
		stringstream str(line);
		for(int i=0;i<3;i++)
			getline(str,tokens[i],',');
		// put in student object
		eligibilitie obj(tokens[0], tokens[1], stoi(tokens[2]));
		// add students(vector) obj in vector
		eligibilities.push_back(obj);
	}


}

