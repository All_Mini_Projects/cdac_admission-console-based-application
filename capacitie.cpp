/*
 * capacitie.cpp
 *
 *  Created on: 18-Apr-2020
 *     Author: sunbeam
 */

#include<iostream>
#include<fstream>
#include<vector>
#include<sstream>
#include<iomanip>
#include"header.h"
using namespace std;


capacitie::capacitie()
{
	center_id=" ";
	string course_name=" ";
	capacity=0;
	filled_capcity=0;
}


capacitie::capacitie(string center_id,string course_name,
		int capacity,int filled_capcity)
{
	this->center_id=center_id;
	this->course_name=course_name;
	this->capacity=capacity;
	this->filled_capcity=filled_capcity;
}


void capacitie::display_cap()
{

	//cout<<this->center_id<<" ,";
	cout<<setw(15)<<this->course_name<<"  ";
	cout<<setw(10)<<this->capacity<<"  ";
	cout<<setw(20)<<this->filled_capcity;
}

string capacitie::get_capa_center_id()
{
	return this->center_id;
}

string capacitie::get_capa_course_name()
{
	return this->course_name;
}

int capacitie::get_capa_center_capacity()
{
	return this->capacity;
}
int capacitie::get_capa_center_filled_capcity()
{
	return this->filled_capcity;
}

void capacitie::set_capa_center_filled_capcity(int filled_capcity)
{
	this->filled_capcity=filled_capcity;
}

//================== ** Global Funtions ** ==================


//searching student in student vector

capacitie* search_for_center(vector<capacitie>& capacities, string center) {
	unsigned i;
	for(i=0; i<capacities.size(); i++) {
		if(capacities[i].get_capa_center_id() == center)
		{
			return &capacities[i];
		}
		}
	return 0;
}

// Loading function for capacities.csv

void load_cpacitie_csv(vector<capacitie>& capa)
{
	ifstream fin;
	fin.open("capacities.csv");
	if(!fin)
	{
		cout<<"\nFailed to open capacities.csv file"<<endl;
		return;
	}

	string line;
	while(getline(fin,line))
	{
		string tokens[4];
		stringstream str(line);
		for(int i=0;i<4;i++)
			getline(str,tokens[i],',');

		capacitie obj(tokens[0],tokens[1],stoi(tokens[2]),stoi(tokens[3]));
		capa.push_back(obj);
	}
}


// function to Write in capacities.csv

void save_capacities_csv(vector<capacitie>& capacities) {
	unsigned i;
	ofstream fout("new_capacities.csv", ios::out | ios::trunc);
	// check
	for(i=0; i<capacities.size(); i++) {
		capacitie *c = &capacities[i];
		fout<<c->center_id<<","
			<<c->course_name<<","
			<<c->capacity<<","
			<<c->filled_capcity<<endl;
	}

}

