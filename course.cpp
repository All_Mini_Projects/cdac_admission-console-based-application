/*
 * course.cpp
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */



#include<iostream>
#include"header.h"
#include<sstream>
#include<fstream>
#include<iomanip>
#include<vector>

using namespace std;


course::course()
{
	Id=0;
	course_name=" ";
	fees=0.0;
	section=" ";
}
course::course(int Id,string course_name,double fees,string section)
{
	this->Id=Id;
	this->course_name=course_name;
	this->fees=fees;
	this->section=section;
}
void course::accepct()
{
	cout<<"\nEnter course Id :";
	cin>>Id;
	cout<<"\nEnter course name :";
	cin>>course_name;
	cout<<"\nEnter course fees :";
	cin>>fees;
	cout<<"\nEnter section :";
	cin>>section;
}

void course::display()
{
	cout<<"ID "<<setw(15)<<"Course Name"<<setw(10)<<"Fee"<<setw(20)<<"Eligible section"<<endl;
	cout<<setw(3)<<this->Id;
	cout<<setw(15)<<this->course_name;
	cout<<setw(10)<<this->fees;
	cout<<setw(10)<<right<<this->section;
	cout<<endl;
}

void course::display_eligib()
{
	cout<<endl;
	//cout<<setw(15)<<"    Course Name "<<setw(25)<<"Degree"<<setw(13)<<"Min. marks"<<endl;
	cout<<"     Eligible Degree"<<setw(13)<<"Min. marks"<<endl;

	for(unsigned int i=0;i<eligib.size();i++)
	{

		cout<<"    -";
		eligib[i].display_elig();
		cout<<endl;

	}
	cout<<endl;
}

string course::get_course_name()
{
	return this->course_name;
}

string course::get_course_section()
{
	return this->section;
}
//==================== ** Global functions **=========================

//finding course

course* find_course(vector<course>& courses,string course_name)
{
	unsigned i;
	for(i=0; i<courses.size(); i++) {
			if(courses[i].get_course_name() == course_name)
				return &courses[i];
		}
		return NULL;
}

//loading functions for course.cvs

void load_course_csv(vector<course>& cousres)
{
	ifstream fin;
	fin.open("courses.csv");
	if(!fin)
	{
		cout<<"Filed to open course.csv file "<<endl;
		return ;
	}
	string line;
	while(getline(fin,line))
	{
			string tokens[4];
			stringstream str(line);
			for(int i=0;i<4;i++)
				getline(str,tokens[i],',');
			course obj(stoi(tokens[0]),tokens[1],stod(tokens[2]),tokens[3]);

			cousres.push_back(obj);
	}
}

//loading eliginlities.csv in course vector

void load_eligiblities_csv(vector<course>& courses)
{
	ifstream fin;
	fin.open("eligibilities.csv");
	if(!fin)
	{

		cout<<"Filed to open eligiblities.csv file "<<endl;
		return ;
	}

	string line;
	while(getline(fin,line))
	{
		string tokens[3];
		stringstream str(line);
		for(int i=0;i<3;i++)
			getline(str,tokens[i],',');
		eligibilitie obj(tokens[0],tokens[1],stod(tokens[2]));

		course *c=find_course(courses,obj.get_eligib_course_name());
		c->eligib.push_back(obj);

	}
}
