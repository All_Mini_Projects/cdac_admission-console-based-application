 /*
 * main.cpp
 *
 *  Created on: 17-Apr-2020
 *      Author: sunbeam
 */


#include<iostream>
#include"header.h"
#include<sstream>
#include<iomanip>
#include<cstring>
#include<vector>


using namespace std;


vector<preference> preferences;
vector<capacitie> capa;
vector<course> courses;
vector<center> centers;
vector<student> students;				//all needed vector
vector<eligibilitie> eligibilities;

int main()
{

	::load_student_csv(students);
	::load_preference_csv(students);

	::load_cpacitie_csv(capa);
	::load_pref2_csv(preferences);
	//student s1;

	::load_course_csv(courses);
	::load_eligiblities_csv(courses);
	::load_center_csv(centers);
	::load_capacitie_csv(centers);					//loading all files
	::load_student_csv(eligibilities);
	string Name,pass,ID;

	//  testing
//	::new_student_registration(students);
//	::save_student_csv(students);
//	cout<<students.size();

//	::add_preference(preferences);
//	::add_preference_spacific(students,preferences,eligibilities);
//	::save_preferences_csv(preferences);

//	cout<<"\nThis are courses your are eligible "<<endl;
//	::display_eligibal_courses_name(students,eligibilities,"rohit");
//	::save_preferences_csv(preferences);

//	::display_eligible_courses_and_center(students,eligibilities,centers,"aditi");



	while(1)
	{
		main:
		int choice=::menu();				//calling main menu
		switch(choice)
		{
		case 1: 					//Admin
		{

			cout<<"-----------------------"<<endl;
			cout<<"***** Admin Login *****"<<endl;
			cout<<"-----------------------"<<endl;
			login:
			cout<<"Enter username :";
			cin>>Name;
			string USERNAME="admin";
					if(Name!=USERNAME) 					//checking user name
					{
						cout<<"Make sure that you entered USERNAME is correct"<<endl;
						goto login;
					}
					else
					{
						pass:
						cout<<"\nEnter your password :";
						cin>>pass;
						if(pass!=Name)				//checking user password
						{
							cout<<"\nEnter correct password(hint :username) ";
							goto pass;
						}
						else
						{
							goto admin;				//jumping on admin sub menu
						}
					}

		}
		break;
		case 2:													//Center Login
		{
			cout<<"-----------------------"<<endl;
			cout<<"***** Center Login *****"<<endl;
			cout<<"-----------------------"<<endl;
			center:
			cout<<"Enter Center ID :";
			cin>>ID;
			int id=::check_center_ID(centers,ID);				//checking user name
			if(id==0)
			{
				cout<<"Make sure that you entered center ID is correct"<<endl;
				goto center;
			}
			else
			{
				int pass_check;
				cen_pass:
				cout<<"\nEnter Your Password :";
				cin>>pass;
				pass_check=::check_center_pass(centers,pass);		//checking user password
				if(pass_check==0)
				{
					cout<<"\nEnter Center Password Is Wrong ";
					goto cen_pass;
				}
				else
				{
					goto c_menu;								//jumping on admin sub menu
				}
			}

		}
			break;
		case 3:						//Student Login
		{
			cout<<"-----------------------"<<endl;
			cout<<"***** Student Login *****"<<endl;
			cout<<"-----------------------"<<endl;
			student_login:
			cout<<"Enter Your USERNAME :";
			cin>>Name;
			int check=check_student(students,Name);				//checking user name
			if (check==0)
			{
				cout<<"Make sure that you entered USERNAME is correct"<<endl;
				goto student_login;
			}
			else
			{
				student_pass:
				cout<<"\nEnter your password :";
				cin>>pass;
				if(pass!=Name)									//checking user password
				{
					cout<<"\nEnter correct password(hint :username) ";
					goto student_pass;
				}
				else
				{
					::student_data(students,Name);
					goto sub;								//jumping   student sub menu
				}
			}
		}
		break;
		case 4:												//student register
		{
			::new_student_registration(students);
			::save_student_csv(students);

			goto main;
		}
			break;
		default :
			cout<<"\nYou Enter Wrong Key Try Again :"<<endl;
			break;

		}//end of switch of main menu

	}//end of while of main menu





//================** Admin menu **===========================


	while(1)    //admin sub menu
	{
		admin:
		int choice=::admin_menu();						//calling admin sub menu
		switch(choice)
		{
		case 1:  	 									//List courses & eligibilities
		{
			for(unsigned int i=0;i<courses.size();i++)
			{
				courses[i].display();
				//cout<<endl;
				courses[i].display_eligib();
			}

		}

			break;
		case 2:											//List centers & capacities
		{
			for(unsigned int i=0;i<centers.size();i++)
			{

				centers[i].display_center();
				cout<<endl;
				centers[i].display_capacitie();
			}
		}
			break;
		case 3:												//List students
		{
			for(unsigned i=0;i<students.size();i++)
			{
				students[i].display_student();
				cout<<endl;
			}
		}
			break;
		case 4:												//Update student ranks
		{
			int F_number;
			cout<<"\nEnter Student Form No :";
			cin>>F_number;
			::update_students_rank(students,F_number);

		}

			break;
		case 5:												//Allocate centers (Round 1)
		{

			::set_allocate_round_1_(students,preferences,courses,centers,capa);
			::save_student_csv(students);
			::save_capacities_csv(capa);
			cout<<"\nAllocation Of Round One is Done "<<endl;

		}
			break;
		case 6:												//Allocate centers (Round 2)
		{

			for(int i=0;i<22;i++)							//updating few payment to check code flow (not code part)
			{
				::update_payment_for_use(students);//106 99 11 4 108 75 37 116 36 101 54 168 56 102 14 86 169 26 97 126 139 127
			}

			::before_round_2(students,capa);
			::save_student_csv(students);					 //saving in students and capacities files
			::save_capacities_csv(capa);
			char one;
			cout<<"\nPress Y "<<endl;						//this is also to check code flow
			cin>>one;

			::set_allocate_round_1_(students,preferences,courses,centers,capa);
			cout<<"\nAllocation Of Round Two is Done "<<endl;
			::save_student_csv(students);
			::save_capacities_csv(capa);     				 //saving in students and capacities files

		}

			break;

		case 7:												//List allocated students
		{

			for(unsigned i=0;i<students.size();i++)
			{
				if(students[i].get_allocated_preference()!=0)
				{
				students[i].display_student();
				cout<<endl;
				}
			}

		}
			break;
		case 8:												//List paid students
		{

			for(unsigned i=0;i<students.size();i++)
			{
				if((students[i].get_payment_status()!=0)
						&&(students[i].get_allocated_preference()!=0))
				{
					students[i].display_student();
					cout<<endl;
				}
			}

		}

			break;
		case 9:												//List reported (at center) students

		{

			for(unsigned i=0;i<students.size();i++)
			{
				if(students[i].get_reporting_status()!=0)
				{
					students[i].display_student();
					cout<<endl;
				}
			}

		}


			break;
		case 10:											//Generate PRN
		{

			string course,center,prn;
			int count=0;
			for(unsigned i=0;i<students.size();i++)			//updating all allocated students payment
			{												//and report .. just to check code flow(not code part)
				if(students[i].get_allocated_preference()!=0)
				{
					students[i].set_payment(1800);
					students[i].set_reporting_status(1);
				}
			}

			for(unsigned i=0;i<students.size();i++)			//generating prn for reported and paid students
			{
				if((students[i].get_payment_status()>0)
						&&(students[i].get_reporting_status()==1))
				{
					count=count+1;
					course=students[i].get_allocated_course_name();
					center=students[i].get_allocated_center_id();

						prn=course+"_";
						prn=prn+center;						//creating_prn
						prn=prn+"_";						//e.g.PG-DMC_SIP_1
						prn=prn+to_string(count);

					//cout<<prn<<endl;
					students[i].set_prn(prn);				//setting students prn
				}
			}
			::save_student_csv(students);

		}


			break;
		case 11:											//List admitted students (with PRN) for given center
		{

			for(unsigned i=0;i<students.size();i++)
			{
				if(students[i].get_reporting_status()!=0)
				{
					students[i].display_student();
					cout<<endl;
				}
			}

		}
			break;
		case 12:											//log out
			goto main;
			break;
		default :
			cout<<"\nEnter wrong key "<<endl;
			break;

		}//end of switch case of admin sub menu


	}//end of while of admin sub menu






//=============== **  Center Sub Menu  ** =============================
	while(1)
	{

		c_menu:
		int choice=:: center_menu();				//calling center sub menu
		switch(choice)
		{
		case 1:  									//List courses (of that center) with capacities
		{
			cout<<setw(16)<<"Course "<<setw(18)<<"Capacities "<<setw(12)<<"Filled"<<endl;
			for(unsigned i=0;i<capa.size();i++)
			{
				if(capa[i].get_capa_center_id()==ID)
				{
					capa[i].display_cap();
					cout<<endl;
				}
			}

		}
			break;
		case 2:										//List students (allocated to that center)
		{

			for(unsigned i=0;i<students.size();i++)
			{
				if(students[i].get_allocated_center_id()==ID)
				{
					students[i].display_student();
					cout<<endl;
				}
			}


		}
			break;
		case 3:										//Update reported status
		{

			unsigned i;
			{
				int number;
				cout<<"\nEnter student form number (ID) :";
				cin>>number;
				for(i=0; i<students.size(); i++) {
					if(students[i].get_payment_status()!=0)
					{
						if(students[i].get_form_no() == number) {
							students[i].set_reporting_status(1);
							::save_student_csv(students);
							cout<<"\nStudent report status updated "<<endl;
						}
					}
				}

		}
			break;
		case 4:										//List admitted students (with PRN)
		{

			for(unsigned i=0;i<students.size();i++)
			{
				if((students[i].get_allocated_center_id()==ID)
						&&students[i].get_reporting_status()==1)
				{
					students[i].display_student();
					cout<<endl;
				}
			}

		}
			break;
		case 5:										//Log out
			goto main;
			break;
		default :
			cout<<"\nYou enter choice is wrong plz try one more time "<<endl;
			goto c_menu;
			break;
		}

	}//end of switch case of center

}//end of while of center sub menu








//==================== ** Student Sub Menu ** ========================
	while(1)
	{
		sub:
				int sub_choice=student_sub_menu();			//calling sub menu of students
				switch(sub_choice)
				{
				case 1:
				{

						int payment;						//update student payment

						for(unsigned i=0; i<students.size(); i++) {
							if(students[i].get_name() == Name)
							{


								cout<<"\nYour total paid fee  is :";
								cout<<students[i].get_payment_status()<<endl;

								cout<<"\nEnter your payment amount :";
								cin>>payment;
								students[i].set_payment(payment);

							}
						}
						::save_student_csv(students);
						cout<<"\nYour payment amount is updated "<<endl;
					cout<<endl;
				}

					break;
				case 2:									//Add Preference
				{

//					cout<<"\nThis are courses your are eligible "<<endl;
//					::display_eligible_courses_name(students,eligibilities,Name);

					cout<<"\nThis are some center which offer the course/s :"<<endl;
					::display_eligible_courses_and_center(students, eligibilities,centers,capa,Name);

					cout<<endl;
					::add_preference_spacific(students,preferences,eligibilities);     	//add preference at specific position
					::save_preferences_csv(preferences);
					cout<<endl;

				}
					break;
				case 3:  								//List of course you eligible for
				{
					cout<<endl;

					cout<<"\nThis are courses your are eligible "<<endl;
					::display_eligible_courses_name(students,eligibilities,Name);
					cout<<endl;

				}
					break;
				case 4:									//List all center
				{
					cout<<endl;

					cout<<setw(12)<<left<<"Center Id"<<setw(25)<<left<<"Center Name"<<setw(18)<<left<<"address"
							<<setw(15)<<left<<"coordinator"<<setw(12)<<left<<"password"<<endl;
					for(unsigned int i=0;i<centers.size();i++)
					{

						centers[i].display_center();
						cout<<endl;
					}
					cout<<endl;
				}

					break;
				case 5:									//See Allocated center and course
				{
					cout<<endl;
					for(unsigned i=0;i<students.size();i++)
					{
						if(students[i].get_name()==Name)
						{
							cout<<students[i].get_allocated_preference()<<"   ";
							cout<<students[i].get_allocated_course_name()<<"   ";
							cout<<students[i].get_allocated_center_id()<<endl;
						}
					}
					cout<<endl;
				}

					break;
				case 6:
					goto main;
					break;
				default :
					cout<<"\nYou Enter Wrong Key "<<endl;
					break;

				}//end of switch case of students

	}//end of while of students sub menu


	return 0;
}
