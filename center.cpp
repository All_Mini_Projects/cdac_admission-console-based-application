/*
 * center.cpp
 *
 *  Created on: 18-Apr-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<fstream>
#include<vector>
#include<iomanip>
#include<sstream>
#include<algorithm>
#include"header.h"
using namespace std;


center::center()
{
	center_id=" ";
	center_name=" ";
	address=" ";
	coordinator=" ";
	password=" ";
}
center::center(string center_id,string center_name,string address,
		string coordinator,string password)
{
	this->center_id=center_id;
	this->center_name=center_name;
	this->address=address;
	this->coordinator=coordinator;
	this->password=password;
}
void center::display_center()
{

	cout<<setw(12)<<left<<this->center_id;
	cout<<setw(25)<<left<<this->center_name;
	cout<<setw(18)<<left<<this->address;
	cout<<setw(15)<<left<<this->coordinator;
	cout<<setw(12)<<left<<this->password;
}
void center::display_capacitie()
{

	cout<<endl;
	cout<<"   ";
	cout<<setw(15)<<left<<"course name"<<setw(10)<<"capacity"<<setw(20)<<"filled_capcity"<<endl;
	unsigned i;
	for(i=0;i<capa.size();i++)
	{
		cout<<"   -";
		capa[i].display_cap();
		cout<<endl;
	}
	cout<<endl;
}
string center::get_center_id()
{
	return this->center_id;
}
string center::get_center_pass()
{
	return this->password;
}

string center::get_center_name()
{
	return this->center_name;
}


//===============** Globe Functions **===============================

//===============** Menu **===============================

int menu()
{
	int choice;
	cout<<" **  Menu **"<<endl;
	cout<<"1.Admin login"<<endl;
	cout<<"2.Center Login"<<endl;
	cout<<"3.Student Login"<<endl;
	cout<<"4.New Student Registration "<<endl;
	cout<<"Enter your choice :";
	cin>>choice;
	return choice;
}

int admin_menu()
{
	int choice;

	cout<<"1.List courses & eligibilities"<<endl;
	cout<<"2.List centers & capacities"<<endl;
	cout<<"3.List students"<<endl;
	cout<<"4.Update student ranks"<<endl;
	cout<<"5.Allocate centers (Round 1)"<<endl;
	cout<<"6.Allocate centers (Round 2)"<<endl;
	cout<<"7.List allocated students"<<endl;
	cout<<"8.List paid students"<<endl;
	cout<<"9.List reported (at center) students"<<endl;
	cout<<"10.Generate PRN"<<endl;
	cout<<"11.List admitted students (with PRN) for given center"<<endl;
	cout<<"12.log out "<<endl;
	cout<<"Enter your choice :";
	cin>>choice;
	return choice;
}

int center_menu()
{
	int choice=0;
	cout<<"1.List courses (of that center)"<<endl;
	cout<<"2.List students (allocated to that center)"<<endl;
	cout<<"3.Update reported status"<<endl;
	cout<<"4.List admitted students (with PRN)"<<endl;
	cout<<"5.Log out"<<endl;
	cin>>choice;
	return choice;
}


// finding center in vector

center* find_center(vector<center>& centers,string center_id)
{

	unsigned i;
	for(i=0;i<centers.size();i++)
	{
		if(centers[i].get_center_id()==center_id)
			return &centers[i];
	}
	return NULL;
}

// function to display center with capacity

center* center_data(vector<center>& centers, string ID) {
	unsigned i;
	for(i=0; i<centers.size(); i++) {
		if(centers[i].get_center_id() == ID)
		{
			centers[i].display_center();
			cout<<endl;
			centers[i].display_capacitie();
		}
	}
	return NULL;
}

// checking center ID is present or not

int check_center_ID(vector<center>& centers, string Name) {
	unsigned i;
	for(i=0; i<centers.size(); i++) {
		if(centers[i].get_center_id() == Name)
			return 1;
		}
	return 0;
}


// checking center password is correct or not

int check_center_pass(vector<center>& centers, string pass) {
	unsigned i;
	for(i=0; i<centers.size(); i++) {
		if(centers[i].get_center_pass() == pass)
			return 1;
		}
	return 0;
}


// loading function for center.csv

void load_center_csv(vector<center>& centers)
{
	ifstream fin;
	fin.open("centers.csv");
	if(!fin)
	{
		cout<<"\nFailed to open centers.csv file"<<endl;
		return;
	}

	string line;
	while(getline(fin,line))
	{
		string tokens[5];
		stringstream str(line);
		for(int i=0;i<5;i++)
			getline(str,tokens[i],',');

		center obj(tokens[0],tokens[1],tokens[2],tokens[3],tokens[4]);
		centers.push_back(obj);
	}
}

// Loading capacities in center

void load_capacitie_csv(vector<center>& centers)
{
	ifstream fin;
	fin.open("capacities.csv");
	if(!fin)
	{
		cout<<"\nFailed to open capacities.csv"<<endl;
		return;
	}
	string line;
	while(getline(fin,line))
	{
		string tokens[4];
		stringstream str(line);
		for(int i=0;i<4;i++)
			getline(str,tokens[i],',');
		capacitie obj(tokens[0],tokens[1],stoi(tokens[2]),stoi(tokens[3]));

		center *c=find_center(centers,obj.get_capa_center_id());        //loading capacity in center vector
		c->capa.push_back(obj);

	}
}


//========================= ** Admin Function ** ====================================


//round 1 allocation function

void set_allocate_round_1_(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa)
{
	sort(students.begin(),students.end(),comp_rank_A);
	int filled=0;
	unsigned i,j,k,s;
	int num=0;
	while(num<10)
			{


			for(i=0;i<students.size();i++)
			{
				for(j=0;j<preferences.size();j++)
				{
					if((students[i].get_form_no()==preferences[j].get_student_no())
							&&(students[i].get_allocated_preference()==0)
							&&(students[i].get_rank_A()!=-1))
					{
						if((preferences[j].get_pref_no()==num+1)
								&&(preferences[j].get_course_name()=="PG-DGI")
								&&(students[i].get_payment_status()>=0))
						{
							/*
						cout<<students[i].get_form_no()<<"   ";
						cout<<preferences[j].get_pref_no()<<"  ";
						cout<<preferences[i].get_course_name();
						cout<<endl;
						*/
					for(k=0;k<courses.size();k++)
					{
						string ch="A";
						if(courses[k].get_course_section().compare(ch)==0)
						{
							for(s=0;s<capa.size();s++)			//loop for capacities.csv
							{
								string center=preferences[j].get_center_id();
								if(capa[s].get_capa_center_id()==preferences[j].get_center_id())
								{
									if(capa[s].get_capa_center_capacity() > capa[s].get_capa_center_filled_capcity())
									{													//add only if filled capacity is !=center capacity
										if((capa[s].get_capa_course_name()=="PG-DGI")
												&&(students[i].get_allocated_preference()==0)
												&&(students[i].get_rank_A()!=-1)
												&&(students[i].get_payment_status()>=0))   //adding only if course available
										{

											string id=capa[s].get_capa_center_id();				//center add for student
											students[i].set_allocated_center_id(id);

											string allocated_course_name=capa[s].get_capa_course_name();  //allocated center name
											students[i].set_allocated_course_name(allocated_course_name);

											int allocated_preference=num+1;
											students[i].set_allocated_preference(allocated_preference);  //allocated_preference

												//checking
											//cout<<students[i].get_form_no()<<"  ";
											//cout<<capa[s].get_capa_center_id()<<"  ";


													//Increasing center filled capacitie by one
											filled=capa[s].get_capa_center_filled_capcity()+1;			//after student center allocation
											//cout<<"filled :"<<filled<<endl;							//increasing count filled capacity of that center

											capa[s].set_capa_center_filled_capcity(filled);

										}
									}

								}
							}

						}

					}

						}
					}
				}
			}
			++num;
			}


::set_allocate_round_1_B(students,preferences,courses,centers,capa);
}


void set_allocate_round_1_B(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa)
{
	sort(students.begin(),students.end(),comp_rank_B);
		int filled=0;
		unsigned i,j,k,s;
		int num=0;
		while(num<10)
				{


				for(i=0;i<students.size();i++)
				{
					for(j=0;j<preferences.size();j++)
					{
						if((students[i].get_form_no()==preferences[j].get_student_no())
								&&(students[i].get_allocated_preference()==0)
								&&(students[i].get_rank_A()!=-1)
								&&(students[i].get_payment_status()>=0))
						{
							if((preferences[j].get_pref_no()==num+1)
									&&(preferences[j].get_course_name()=="PG-DAC"))
							{
							/*  // checking
								cout<<students[i].get_form_no()<<"   ";
								cout<<preferences[j].get_pref_no()<<"  ";
								cout<<preferences[i].get_course_name();
								cout<<endl;
							*/
						for(k=0;k<courses.size();k++)
						{
							string ch="B";
							if(courses[k].get_course_section().compare(ch)==0)
							{
								for(s=0;s<capa.size();s++)			//loop for capacities.csv
								{
									string center=preferences[j].get_center_id();
									if(capa[s].get_capa_center_id()==preferences[j].get_center_id())
									{
										if(capa[s].get_capa_center_capacity() > capa[s].get_capa_center_filled_capcity())
										{													//add only if filled capacity is !=center capacity
											if((capa[s].get_capa_course_name()=="PG-DAC")
													&&(students[i].get_allocated_preference()==0)
													&&(students[i].get_rank_A()!=-1))   //adding only if course available
											{


												string id=capa[s].get_capa_center_id();				//center add for student
												students[i].set_allocated_center_id(id);

												string allocated_course_name=capa[s].get_capa_course_name();  //allocated center name
												students[i].set_allocated_course_name(allocated_course_name);

												int allocated_preference=num+1;
												students[i].set_allocated_preference(allocated_preference);  //allocated_preference

													// checking
												//cout<<students[i].get_form_no()<<"  ";
												//cout<<capa[s].get_capa_center_id()<<"  ";


												// Increasing center filled capacitie by one
												filled=capa[s].get_capa_center_filled_capcity()+1;       //after student center allocation
												//cout<<"filled :"<<filled<<endl;						//increasing count filled capacity of that center

												capa[s].set_capa_center_filled_capcity(filled);

											}
										}

									}
								}

							}

						}

							}
						}
					}
				}
				++num;
				}
::set_allocate_round_1_B2(students,preferences,courses,centers,capa);
::set_allocate_round_1_B3(students,preferences,courses,centers,capa);

}


void set_allocate_round_1_B2(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa)
{
	sort(students.begin(),students.end(),comp_rank_B);
	int filled=0;
	unsigned i,j,k,s;
	int num=0;
	while(num<10)
			{


			for(i=0;i<students.size();i++)
			{
				for(j=0;j<preferences.size();j++)
				{
					if((students[i].get_form_no()==preferences[j].get_student_no())
							&&(students[i].get_allocated_preference()==0)
							&&(students[i].get_rank_A()!=-1)
							&&(students[i].get_payment_status()>=0))
					{
						if((preferences[j].get_pref_no()==num+1)
								&&(preferences[j].get_course_name()=="PG-DMC"))
						{
							/*
							cout<<students[i].get_form_no()<<"   ";
							cout<<preferences[j].get_pref_no()<<"  ";
							cout<<preferences[i].get_course_name();
							cout<<endl;
							*/
					for(k=0;k<courses.size();k++)
					{
						string ch="B";
						if(courses[k].get_course_section().compare(ch)==0)
						{
							for(s=0;s<capa.size();s++)			//loop for capacities.csv
							{
								string center=preferences[j].get_center_id();
								if(capa[s].get_capa_center_id()==preferences[j].get_center_id())
								{
									if(capa[s].get_capa_center_capacity() > capa[s].get_capa_center_filled_capcity())
									{													//add only if filled capacity is !=center capacity
										if((capa[s].get_capa_course_name()=="PG-DMC")
											&&(students[i].get_allocated_preference()==0)
											&&(students[i].get_rank_A()!=-1))//adding only if course available
										{

											string id=capa[s].get_capa_center_id();				//center add for student
											students[i].set_allocated_center_id(id);

											string allocated_course_name=capa[s].get_capa_course_name();  //allocated center name
											students[i].set_allocated_course_name(allocated_course_name);

											int allocated_preference=num+1;
											students[i].set_allocated_preference(allocated_preference);  //allocated_preference

												//	checking
											//cout<<students[i].get_form_no()<<"  ";
											//cout<<capa[s].get_capa_center_id()<<"  ";


												// Increasing center filled capacitie by one
											filled=capa[s].get_capa_center_filled_capcity()+1;      	//after student center allocation
											//cout<<"filled :"<<filled<<endl;							//increasing count filled capacity of that center

											capa[s].set_capa_center_filled_capcity(filled);

										}
									}

								}
							}

						}

					}

						}
					}
				}
			}
			++num;
			}
}


void set_allocate_round_1_B3(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa)
{
	sort(students.begin(),students.end(),comp_rank_B);
	int filled=0;
	unsigned i,j,k,s;
	int num=0;
	while(num<10)
			{
		  	  	  	  //sorting by rank B

			for(i=0;i<students.size();i++)					//loop for student vector
			{
				for(j=0;j<preferences.size();j++)			//loop for preference vector
				{
					if((students[i].get_form_no()==preferences[j].get_student_no())
							&&(students[i].get_allocated_preference()==0))
					{
						if((preferences[j].get_pref_no()==num+1)
								&&(preferences[j].get_course_name()=="PG-DBDA")
								&&(students[i].get_rank_A()!=-1)
								&&(students[i].get_payment_status()>=0))
						{
						/*
							cout<<students[i].get_form_no()<<"   ";
							cout<<preferences[j].get_pref_no()<<"  ";
							cout<<preferences[i].get_course_name();
							cout<<endl;
						*/
					for(k=0;k<courses.size();k++)
					{
						string ch="B";
						if(courses[k].get_course_section().compare(ch)==0)
						{
							for(s=0;s<capa.size();s++)			//loop for capacities.csv
							{
								string center=preferences[j].get_center_id();
								if(capa[s].get_capa_center_id()==preferences[j].get_center_id())
								{
									if(capa[s].get_capa_center_capacity() > capa[s].get_capa_center_filled_capcity())
									{													//add only if filled capacity is !=center capacity
										if((capa[s].get_capa_course_name()=="PG-DBDA")
												&&(students[i].get_allocated_preference()==0)
												&&(students[i].get_rank_A()!=-1)
												&&(students[i].get_payment_status()>=0))   //adding only if course available
										{

											string id=capa[s].get_capa_center_id();				//center add for student
											students[i].set_allocated_center_id(id);

											string allocated_course_name=capa[s].get_capa_course_name();  //allocated center name
											students[i].set_allocated_course_name(allocated_course_name);

											int allocated_preference=num+1;
											students[i].set_allocated_preference(allocated_preference);  //allocated_preference

												//checking
											//cout<<students[i].get_form_no()<<"  ";
											//cout<<capa[s].get_capa_center_id()<<"  ";


													//Increasing center filled capacitie by one
											filled=capa[s].get_capa_center_filled_capcity()+1;          //after student center allocation
											//cout<<"filled :"<<filled<<endl;							//increasing count filled capacity of that center

											capa[s].set_capa_center_filled_capcity(filled);

										}
									}

								}
							}

						}

					}

						}
					}
				}
			}
			++num;
			}

::set_allocate_round_1_C(students,preferences,courses,centers,capa);
}

void set_allocate_round_1_C(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa)
{

	sort(students.begin(),students.end(),comp_rank_C);

	int filled=0;
	unsigned i,j,k,s;
	int num=0;
	while(num<10)
			{


			for(i=0;i<students.size();i++)
			{
				for(j=0;j<preferences.size();j++)
				{
					if((students[i].get_form_no()==preferences[j].get_student_no())
							&&(students[i].get_allocated_preference()==0)
							&&(students[i].get_rank_A()!=-1)
							&&(students[i].get_payment_status()>=0))
					{
						if((preferences[j].get_pref_no()==num+1)
								&&(preferences[j].get_course_name()=="PG-DESD"))
						{
							/*
						cout<<students[i].get_form_no()<<"   ";
						cout<<preferences[j].get_pref_no()<<"  ";
						cout<<preferences[i].get_course_name();
						cout<<endl;
						*/
					for(k=0;k<courses.size();k++)
					{
						string ch="C";
						if(courses[k].get_course_section().compare(ch)==0)
						{
							for(s=0;s<capa.size();s++)			//loop for capacities.csv
							{
								string center=preferences[j].get_center_id();
								if(capa[s].get_capa_center_id()==preferences[j].get_center_id())
								{
									if(capa[s].get_capa_center_capacity() > capa[s].get_capa_center_filled_capcity())
									{													//add only if filled capacity is !=center capacity
										if((capa[s].get_capa_course_name()=="PG-DESD")
												&&(students[i].get_allocated_preference()==0)
												&&(students[i].get_rank_A()!=-1)
												&&(students[i].get_payment_status()>=0))   //adding only if course available
										{
														//checking

											cout<<endl;


											string id=capa[s].get_capa_center_id();				//center add for student
											students[i].set_allocated_center_id(id);

											string allocated_course_name=capa[s].get_capa_course_name();  //allocated center name
											students[i].set_allocated_course_name(allocated_course_name);

											int allocated_preference=num+1;
											students[i].set_allocated_preference(allocated_preference);  //allocated_preference

												//checking
											//cout<<students[i].get_form_no()<<"  ";
											//cout<<capa[s].get_capa_center_id()<<"  ";


													//Increasing center filled capacitie by one
											filled=capa[s].get_capa_center_filled_capcity()+1;          //after student center allocation
											//cout<<"filled :"<<filled<<endl;							//increasing count filled capacity of that center

											capa[s].set_capa_center_filled_capcity(filled);

										}
									}

								}
							}

						}

					}
				}
			}
			}
			}
			++num;
			}
}


//  steps to be done before going for round 2

void before_round_2(vector<student>& students,vector<capacitie>& capa)
{

	for(unsigned i=0;i<capa.size();i++)		//making all filled capacities of all center 0
	{
		capa[i].set_capa_center_filled_capcity(0);
	}


	for(unsigned i=0;i<students.size();i++)		//marking students payment as
	{											//-1 if student allocated not paid

			if((students[i].get_allocated_preference()!=0)
					&&students[i].get_payment_status()==0)
			{
				students[i].set_payment(-1);
			}
	}

	for(unsigned i=0;i<students.size();i++)				//making all students data default
	{
				students[i].set_allocated_preference(0);
				students[i].set_allocated_course_name("NA");
				students[i].set_allocated_center_id("NA");
	}
}



//updating some student payment for testing
//round to algoritham

void update_payment_for_use(vector<student>& students)		//just form testing
{
		int form_num;
		cin>>form_num;
		for(unsigned i=0;i<students.size();i++)
		{
			if(students[i].get_form_no()==form_num)
			{
				students[i].set_payment(1800);
			}
		}
}
