/*
 * header.h
 *
 *  Created on: 17-Apr-2020
 *      Author: sunbeam
 */

#ifndef HEADER_H_
#define HEADER_H_


#include<iostream>
#include<string>
#include<fstream>
#include <sstream>
#include<vector>
using namespace std;


class eligibilitie				// eligibilitie class
{
private:
	string eligib_course_name;
	string eligib_degree;
	double eligib_min_percentage;
public:
	eligibilitie();
	eligibilitie(string course_name,string degree,double min_percentage);
	string get_eligib_degree_name();
	string get_eligib_course_name();
	void accepct_elig();
	void display_elig();
	int get_eligib_persentage();
};
void load_student_csv(vector<eligibilitie>& eligibilities);


class course				//course class
{
private:
	int Id;
	string course_name;
	double fees;
	string section;
public:
	vector<eligibilitie> eligib;
	course();
	course(int Id,string course_name,double fees,string section);
	string get_course_name();
	string get_course_section();
	void display_eligib();
	void accepct();
	void display();
};
void load_course_csv(vector<course>& cousres);
void load_eligiblities_csv(vector<course>& courses);


class capacitie  				//capacitie class
{
private:
	string center_id;
	string course_name;
	int capacity;
	int filled_capcity;
public:
	capacitie();
	capacitie(string center_id,string course_name,int capacity,int filled_capcity);
	void display_cap();
	string get_capa_center_id();
	int get_capa_center_filled_capcity();
	int get_capa_center_capacity();
	string get_capa_course_name();
	void set_capa_center_filled_capcity(int filled_capcity);


	friend void save_capacities_csv(vector<capacitie>& capacities);

};
void load_cpacitie_csv(vector<capacitie>& capa);
capacitie* search_for_center(vector<capacitie>& capacities, string center);



class center							//center class
{
private:
	string center_id;
	string center_name;
	string address;
	string coordinator;
	string password;
public:
	vector<capacitie> capa;
	center();
	center(string center_id,string center_name,string Address,string Coordinator,string password);
	void display_center();
	string get_center_id();
	void display_capacitie();
	string get_center_pass();
	string get_center_name();



};

void load_center_csv(vector<center>& centers);
center* find_center(vector<center>& centers,string center_id);
void load_capacitie_csv(vector<center>& centers);
int check_center_ID(vector<center>& centers, string Name);
int check_center_pass(vector<center>& centers, string pass);
void save_capacities_csv(vector<capacitie>& capacities);
int center_menu();
int menu();
int admin_menu();





class preference				//preference class
{
private:
	int student_form_no;
	int preference_no;
	string course_name;
	string center_id;
public:
	preference();
	preference(int form_no,int preference_no,string course_name,string center_id);
	void display();
	void accepct_pref();
	int get_student_no();
	int get_pref_no();
	string get_center_id();
	string get_course_name();
	void set_pref_no(int preference_no);


	friend void save_preferences_csv(vector<preference>& preferences);

};
void load_pref2_csv(vector<preference>& preferences);
void save_preferences_csv(vector<preference>& preferences);




class student				//student class
{
private:
	int Form_no;
	string name;
	int rank_a;
	int rank_b;
	int rank_c;
	string degree;
	double percentage;
	int allocated_preference;
	string allocated_course_name;
	string allocated_center_id;
	double payment;
	int reported_to_center;
	string prn;

public:
	vector<preference> pref;
	student();
	student(int Form_no,const string& name,int rank_a,int rank_b,int rank_c,const string& degree,
			double percentage,int allocated_preference,const string& allocated_course_name,
			const string& allocated_center_id,double payment,int reported_to_center,const string& prn);
	void accept_student();
	void display_student();
	string get_name();
	void display_pref();
	int get_form_no();
	int get_rank_A();
	int get_rank_B();
	int get_rank_C();
	void set_rank_A(int rank_A);
	void set_rank_B(int rank_B);
	void set_rank_C(int rank_C);
	void set_form_num(int form_num);
	int get_payment_status();
	int get_reporting_status();
	void set_payment(int payment);
	void set_allocated_course_name(string allocated_course_name);
	void set_allocated_center_id(string allocated_center_id);
	void set_allocated_preference(int index);
	int get_allocated_preference();
	string get_allocated_center_id();
	string get_allocated_course_name();
	void set_reporting_status(int report);
	void set_prn(string prn);
	string get_prn();
	string get_degree();
	int get_percentage();



	friend void save_student_csv(vector<student>& students);


};

int student_sub_menu();
void load_student_csv(vector<student>& students);
student* student_data(vector<student>& students, string Name);
student* search_for_student2(vector<student>& students, int Form_no);
int check_student(vector<student>& students, string Name);
void load_preference_csv(vector<student>& students);
bool comp_rank_A(student& s1,student& s2);
bool comp_rank_B(student& s1,student& s2);
bool comp_rank_C(student& s1,student& s2);
void update_students_rank(vector<student>& students,int form_num);
void save_student_csv(vector<student>& students);
void new_student_registration(vector<student>& students);
void display_eligible_courses_name(vector<student>& students,
		vector<eligibilitie>& eligibilities,string name);
void display_eligible_courses_and_center(vector<student>& students,
		vector<eligibilitie>& eligibilities,vector<center>& centers,vector<capacitie>& capa,string name);
void add_preference(vector<student>& students,vector<preference>& preferences,
		vector<eligibilitie>& eligibilities);
void add_preference_spacific(vector<student>& students,vector<preference>& preferences,
		vector<eligibilitie>& eligibilities);



void set_allocate_round_1_(vector<student>& students,
			vector<preference>& preferences,vector<course>& courses,
			vector<center>& centers,vector<capacitie>& capa);

void set_allocate_round_1_B(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa);
void set_allocate_round_1_B2(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa);
void set_allocate_round_1_B3(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa);

void set_allocate_round_1_C(vector<student>& students,
		vector<preference>& preferences,vector<course>& courses,
		vector<center>& centers,vector<capacitie>& capa);



void before_round_2(vector<student>& students,vector<capacitie>& capa);
void update_payment_for_use(vector<student>& students);

#endif /* HEADER_H_ */
