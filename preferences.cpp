/*
 * preferences.cpp
 *
 *  Created on: 17-Apr-2020
 *      Author: sunbeam
 */



#include"header.h"
#include<sstream>
#include<cstring>


preference::preference()
{
	student_form_no=0;
	preference_no=0;
	course_name=" ";
	center_id=" ";

}
preference::preference(int form_no,int preference_no,
		string course_name,string center_id)
{
	this->student_form_no=form_no;
	this->preference_no=preference_no;
	this->course_name=course_name;
	this->center_id=center_id;

}
void preference::display()
{
//	cout<<this->student_form_no<<"  ";
	cout<<this->preference_no<<"  ";
	cout<<this->course_name<<"  ";
	cout<<this->center_id<<"  ";
	cout<<endl;
}

void preference::accepct_pref()
{
	cin>>this->student_form_no;
	cin>>this->preference_no;
	cin>>this->course_name;
	cin>>this->center_id;
}

int preference::get_student_no()
{
	return this->student_form_no;
}

string preference::get_course_name()
{
	return this->course_name;
}

int preference::get_pref_no()
{
	return this->preference_no;
}

string preference::get_center_id()
{
	return this->center_id;
}

void preference::set_pref_no(int preference_no)
{
	this->preference_no=preference_no;
}

//====================== ** Global Function ** =============================

//loading preference for allocation work

void load_pref2_csv(vector<preference>& preferences)
{
	ifstream fin;
	fin.open("preferences.csv");
	if(!fin)
	{
		cout<<"\nFailed to open preferences.csv file"<<endl;
		return;
	}

	string line;
	while(getline(fin,line))
	{
		string tokens[4];
		stringstream str(line);
		for(int i=0;i<4;i++)
			getline(str,tokens[i],',');

		preference obj(stoi(tokens[0]),stoi(tokens[1]),tokens[2],tokens[3]);
		preferences.push_back(obj);
	}
}

//saving in preferences.csv

void save_preferences_csv(vector<preference>& preferences) {
	unsigned i;
	ofstream fout("new_preferences.csv", ios::out | ios::trunc);
	// check
	for(i=0; i<preferences.size(); i++) {
		preference *d = &preferences[i];
		fout<<d->student_form_no<<","
			<<d->preference_no<<","
			<<d->course_name<<","
			<<d->center_id<<"\n";
	}
}









